package be.kdg.pro2.ex1OnStage;

import javafx.application.Application;
import javafx.scene.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class OnStage extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		// Set window size to 500x600
		primaryStage.setHeight(600);
		primaryStage.setWidth(500);
		// disallow window resizing
		// primaryStage.setResizable(false);


		primaryStage.setMaxWidth(700);
		// Set a title to the window
		primaryStage.setTitle("On stage");
		primaryStage.show();

		// show a second stage
		Stage secondStage = new Stage();
		// Set a group as the root of the second stage
		Scene secondScene = new Scene(new Group());
		// Set a crosshair cursor to the scene
		secondScene.setCursor(Cursor.CROSSHAIR);
		// Set a yellow background to the scene
		secondScene.setFill(Color.YELLOW);
		secondStage.setScene(secondScene);
		secondStage.show();
	}
}
